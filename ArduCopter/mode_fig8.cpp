#include "Copter.h"
#include <GCS_MAVLink/GCS.h> 

#if MODE_FIG8_ENABLED == ENABLED

/*
 * Init and run calls for stabilize flight mode
 */

/*
 * Init and run calls for circle flight mode
 */

// circle_init - initialise circle controller flight mode
bool ModeFig8::init(bool ignore_checks)
{
//     pilot_yaw_override = false;

//     // initialize speeds and accelerations
//     pos_control->set_max_speed_xy(wp_nav->get_default_speed_xy());
//     pos_control->set_max_accel_xy(wp_nav->get_wp_acceleration());
//     pos_control->set_max_speed_z(-get_pilot_speed_dn(), g.pilot_speed_up);
//     pos_control->set_max_accel_z(g.pilot_accel_z);

//     // initialise circle controller including setting the circle center based on vehicle speed
//     copter.circle_nav->init();

    exec_t_1hz = AP_HAL::millis();
    exec_t_10hz = exec_t_1hz;

    // desired freq = 1Hz -> delay = 1 sec (1000 ms)/freq
    delay_1hz = (uint32_t) ((double)1000.00/(double)1.00);
    delay_10hz = (uint32_t) ((double)1000.00/(double)10.00);

    hal.uartE->begin(57600);

    //g2.test_var31.init();
    copter.hh = 4;  // testing uservariables.h

    return true;
}


// stabilize_run - runs the main stabilize controller
// should be called at 100hz or more
void ModeFig8::run()
{
    // apply simple mode transform to pilot inputs
    update_simple_mode();

    // convert pilot input to lean angles
    float target_roll, target_pitch;
    get_pilot_desired_lean_angles(target_roll, target_pitch, copter.aparm.angle_max, copter.aparm.angle_max);

    // get pilot's desired yaw rate
    float target_yaw_rate = get_pilot_desired_yaw_rate(channel_yaw->get_control_in());

    if (!motors->armed()) {
        // Motors should be Stopped
        motors->set_desired_spool_state(AP_Motors::DesiredSpoolState::SHUT_DOWN);
    } else if (copter.ap.throttle_zero) {
        // Attempting to Land
        motors->set_desired_spool_state(AP_Motors::DesiredSpoolState::GROUND_IDLE);
    } else {
        motors->set_desired_spool_state(AP_Motors::DesiredSpoolState::THROTTLE_UNLIMITED);
    }

    switch (motors->get_spool_state()) {
    case AP_Motors::SpoolState::SHUT_DOWN:
        // Motors Stopped
        attitude_control->set_yaw_target_to_current_heading();
        attitude_control->reset_rate_controller_I_terms();
        break;
    case AP_Motors::SpoolState::GROUND_IDLE:
        // Landed
        attitude_control->set_yaw_target_to_current_heading();
        attitude_control->reset_rate_controller_I_terms();
        break;
    case AP_Motors::SpoolState::THROTTLE_UNLIMITED:
        // clear landing flag above zero throttle
        if (!motors->limit.throttle_lower) {
            set_land_complete(false);
        }
        break;
    case AP_Motors::SpoolState::SPOOLING_UP:
    case AP_Motors::SpoolState::SPOOLING_DOWN:
        // do nothing
        break;
    }

    // call attitude controller
    attitude_control->input_euler_angle_roll_pitch_euler_rate_yaw(target_roll, target_pitch, target_yaw_rate);

    // output pilot's throttle
    attitude_control->set_throttle_out(get_pilot_desired_throttle(),
                                       true,
                                       g.throttle_filt);

    // 10Hz loop
    if (AP_HAL::millis() - exec_t_10hz > delay_1hz) {

        //logging test
        AP::logger().Write("TEST", "TimeUS,ThrIn,ThrOut",
                   "s%%", // units: seconds, meters
                   "F22", // mult: 1e-6, 1e-2
                   "Qff", // format: uint64_t, float
                   AP_HAL::micros64(),
                   attitude_control->get_throttle_in(),
                   motors->get_throttle());

        // update execution time
        exec_t_10hz = AP_HAL::millis();
    }

    // 1Hz loop
    if (AP_HAL::millis() - exec_t_1hz > delay_1hz) {
        // update execution time
        exec_t_1hz = AP_HAL::millis();

        // test outputting to serial
        hal.uartE->printf("param4: %d  \n", copter.hh); // testing uservariables.h

        // output to gcs messages
        gcs().send_text(MAV_SEVERITY_CRITICAL, "hello world! %5.3f", (double)3.142f);

        // g.a_throttle = g.b_throttle - g.c_throttle + g.d_throttle;
        // g.b_throttle += 1;

        // // test params from class 2
        // g2.test_var1 = g2.test_var1 + 1;
        // g2.test_var2 = g2.test_var1 + 1;
        // g2.test_var3 = g2.test_var2 + g2.test_var1 + 1;
        // g2.test_var4 = g2.test_var3 + g2.test_var2 + g2.test_var1 + 1;

        // g2.test_var6 = g2.test_var5 + 1;
        // g2.test_var7 = g2.test_var5 + g2.test_var1 + 1;
        // g2.test_var8 = g2.test_var6 + g2.test_var4 + g2.test_var2 + 1;

        // g2.test_var9 = g2.test_var6 + 3;
        // g2.test_var10 = g2.test_var1 + g2.test_var9;

        // g2.test_var11 = g2.test_var11 + 1;
        // g2.test_var12 = g2.test_var11 + 1;
        // g2.test_var13 = g2.test_var2 + g2.test_var11 + 1;
        // g2.test_var14 = g2.test_var13 + g2.test_var12 + g2.test_var11 + 3;

        // g2.test_var16 = g2.test_var15 + 8;
        // g2.test_var17 = g2.test_var15 + g2.test_var11 + 1;
        // g2.test_var18 = g2.test_var16 + g2.test_var14 + g2.test_var12 + 1;

        // g2.test_var19 = g2.test_var16 + 3;
        // g2.test_var20 = g2.test_var11 + g2.test_var19;

        // g2.test_var21 = g2.test_var21 + 1;
        // g2.test_var22 = g2.test_var21 + 1;
        // g2.test_var23 = g2.test_var22 + g2.test_var21 + 1;
        // g2.test_var24 = g2.test_var23 + g2.test_var22 + g2.test_var21 + 1;

        // g2.test_var26 = g2.test_var25 + 1;
        // g2.test_var27 = g2.test_var25 + g2.test_var21 + 1;
        // g2.test_var28 = g2.test_var26 + g2.test_var4 + g2.test_var2 + 1;

        // g2.test_var29 = g2.test_var26 + 3;
        // g2.test_var30 = g2.test_var21 + g2.test_var29;

        // g2.test_var31 = g2.test_var31 + 1;

        // g2.user_parameters.set_param1Param(3);
        // g2.test_var32 = g2.test_var31 + 2;
        // g2.test_var33 = g2.test_var32 + g2.test_var31 + 1;
        // g2.test_var34 = g2.test_var33 + g2.test_var32 + g2.test_var31 + 3;
        // g2.test_var35 = g2.test_var34 + 2;

        // g2.test_var36 = g2.test_var35 + 2.1;
        // g2.test_var37 = g2.test_var35 + g2.test_var11 + 1;
        // g2.test_var38 = g2.test_var36 + g2.test_var34 + g2.test_var12 + 1;

        // g2.test_var39 = g2.test_var36 + 3;
        // g2.test_var40 = g2.test_var31 + g2.test_var19;

        // g2.test_var41 = g2.test_var41 + 1;
        // g2.test_var42 = g2.test_var41 + 1;
        // g2.test_var43 = g2.test_var42 + g2.test_var41 + 1;
        // g2.test_var44 = g2.test_var43 + g2.test_var2 + g2.test_var41 + 1;
        // g2.test_var45 = g2.test_var25 + 1;
        // g2.test_var46 = g2.test_var45 + 1;
        // g2.test_var47 = g2.test_var45 + g2.test_var1 + 1;
        // g2.test_var48 = g2.test_var6 + g2.test_var44 + g2.test_var2 + 1;

        // g2.test_var49 = g2.test_var46 + 3;
        // g2.test_var50 = g2.test_var41 + g2.test_var9;

        // g2.test_var51 = g2.test_var51 + 1.7;
        // g2.test_var52 = g2.test_var11 + 1.2;
        // g2.test_var53 = g2.test_var52 + g2.test_var11 + 1;
        // g2.test_var54 = g2.test_var53 + g2.test_var12 + g2.test_var11 + 3;

        // g2.test_var56 = g2.test_var15 + 2.1;
        // g2.test_var57 = g2.test_var15 + g2.test_var51 + 1;
        // g2.test_var58 = g2.test_var16 + g2.test_var54 + g2.test_var12 + 1;

        // g2.test_var59 = g2.test_var56 + 3;
        // g2.test_var60 = g2.test_var51 + g2.test_var19;

    }
}

#endif
