#pragma once

#include <AP_Common/AP_Common.h>

#if GRIPPER_ENABLED == ENABLED
 # include <AP_Gripper/AP_Gripper.h>
#endif
#if MODE_FOLLOW_ENABLED == ENABLED
 # include <AP_Follow/AP_Follow.h>
#endif
#if VISUAL_ODOMETRY_ENABLED == ENABLED
 # include <AP_VisualOdom/AP_VisualOdom.h>
#endif

// Global parameter class.
//
class Parameters {
public:
    // The version of the layout as described by the parameter enum.
    //
    // When changing the parameter enum in an incompatible fashion, this
    // value should be incremented by one.
    //
    // The increment will prevent old parameters from being used incorrectly
    // by newer code.
    //
    static const uint16_t        k_format_version = 120;

    // Parameter identities.
    //
    // The enumeration defined here is used to ensure that every parameter
    // or parameter group has a unique ID number.	This number is used by
    // AP_Param to store and locate parameters in EEPROM.
    //
    // Note that entries without a number are assigned the next number after
    // the entry preceding them.	When adding new entries, ensure that they
    // don't overlap.
    //
    // Try to group related variables together, and assign them a set
    // range in the enumeration.	Place these groups in numerical order
    // at the end of the enumeration.
    //
    // WARNING: Care should be taken when editing this enumeration as the
    //			AP_Param load/save code depends on the values here to identify
    //			variables saved in EEPROM.
    //
    //
    enum {
        // Layout version number, always key zero.
        //
        k_param_format_version = 0,
        k_param_software_type, // deprecated
        k_param_ins_old,                        // *** Deprecated, remove with next eeprom number change
        k_param_ins,                            // libraries/AP_InertialSensor variables
        k_param_NavEKF2_old, // deprecated - remove
        k_param_NavEKF2,
        k_param_g2, // 2nd block of parameters
        k_param_NavEKF3,
        k_param_BoardConfig_CAN,
        k_param_osd,

        // simulation
        k_param_sitl = 10,

        // barometer object (needed for SITL)
        k_param_barometer,

        // scheduler object (for debugging)
        k_param_scheduler,

        // relay object
        k_param_relay,

        // (old) EPM object
        k_param_epm_unused,

        // BoardConfig object
        k_param_BoardConfig,

        // GPS object
        k_param_gps,

        // Parachute object
        k_param_parachute,

        // Landing gear object
        k_param_landinggear,    // 18

        // Input Management object
        k_param_input_manager,  // 19

        // Misc
        //
        k_param_log_bitmask_old = 20,           // Deprecated
        k_param_log_last_filenumber,            // *** Deprecated - remove
                                                // with next eeprom number
                                                // change
        k_param_toy_yaw_rate,                   // deprecated - remove
        k_param_crosstrack_min_distance,	// deprecated - remove with next eeprom number change
        k_param_rssi_pin,                   // unused, replaced by rssi_ library parameters
        k_param_throttle_accel_enabled,     // deprecated - remove
        k_param_wp_yaw_behavior,
        k_param_acro_trainer,
        k_param_pilot_speed_up,    // renamed from k_param_pilot_velocity_z_max
        k_param_circle_rate,                // deprecated - remove
        k_param_rangefinder_gain,
        k_param_ch8_option_old, // deprecated
        k_param_arming_check_old,       // deprecated - remove
        k_param_sprayer,
        k_param_angle_max,
        k_param_gps_hdop_good,
        k_param_battery,
        k_param_fs_batt_mah,            // unused - moved to AP_BattMonitor
        k_param_angle_rate_max,         // remove
        k_param_rssi_range,             // unused, replaced by rssi_ library parameters
        k_param_rc_feel_rp,             // deprecated
        k_param_NavEKF,                 // deprecated - remove
        k_param_mission,                // mission library
        k_param_rc_13_old,
        k_param_rc_14_old,
        k_param_rally,
        k_param_poshold_brake_rate,
        k_param_poshold_brake_angle_max,
        k_param_pilot_accel_z,
        k_param_serial0_baud,           // deprecated - remove
        k_param_serial1_baud,           // deprecated - remove
        k_param_serial2_baud,           // deprecated - remove
        k_param_land_repositioning,
        k_param_rangefinder, // rangefinder object
        k_param_fs_ekf_thresh,
        k_param_terrain,
        k_param_acro_rp_expo,
        k_param_throttle_deadzone,
        k_param_optflow,
        k_param_dcmcheck_thresh,        // deprecated - remove
        k_param_log_bitmask,
        k_param_cli_enabled_old,        // deprecated - remove
        k_param_throttle_filt,
        k_param_throttle_behavior,
        k_param_pilot_takeoff_alt, // 64

        // 65: AP_Limits Library
        k_param_limits = 65,            // deprecated - remove
        k_param_gpslock_limit,          // deprecated - remove
        k_param_geofence_limit,         // deprecated - remove
        k_param_altitude_limit,         // deprecated - remove
        k_param_fence,
        k_param_gps_glitch,             // deprecated
        k_param_baro_glitch,            // 71 - deprecated

        // AP_ADSB Library
        k_param_adsb,                   // 72
        k_param_notify,                 // 73

        // 74: precision landing object
        k_param_precland = 74,

        //
        // 75: Singlecopter, CoaxCopter
        //
        k_param_single_servo_1 = 75,    // remove
        k_param_single_servo_2,         // remove
        k_param_single_servo_3,         // remove
        k_param_single_servo_4,         // 78 - remove

        //
        // 80: Heli
        //
        k_param_heli_servo_1 = 80,  // remove
        k_param_heli_servo_2,       // remove
        k_param_heli_servo_3,       // remove
        k_param_heli_servo_4,       // remove
        k_param_heli_pitch_ff,      // remove
        k_param_heli_roll_ff,       // remove
        k_param_heli_yaw_ff,        // remove
        k_param_heli_stab_col_min,  // remove
        k_param_heli_stab_col_max,  // remove
        k_param_heli_servo_rsc,     // 89 = full! - remove

        //
        // 90: misc2
        //
        k_param_motors = 90,
        k_param_disarm_delay,
        k_param_fs_crash_check,
        k_param_throw_motor_start,
        k_param_terrain_follow,    // 94
        k_param_avoid,
        k_param_avoidance_adsb,

        // 97: RSSI
        k_param_rssi = 97,
                
        //
        // 100: Inertial Nav
        //
        k_param_inertial_nav = 100, // deprecated
        k_param_wp_nav,
        k_param_attitude_control,
        k_param_pos_control,
        k_param_circle_nav,
        k_param_loiter_nav,     // 105

        // 110: Telemetry control
        //
        k_param_gcs0 = 110,
        k_param_gcs1,
        k_param_sysid_this_mav,
        k_param_sysid_my_gcs,
        k_param_serial1_baud_old, // deprecated
        k_param_telem_delay,
        k_param_gcs2,
        k_param_serial2_baud_old, // deprecated
        k_param_serial2_protocol, // deprecated
        k_param_serial_manager,
        k_param_ch9_option_old,
        k_param_ch10_option_old,
        k_param_ch11_option_old,
        k_param_ch12_option_old,
        k_param_takeoff_trigger_dz_old,
        k_param_gcs3,
        k_param_gcs_pid_mask,    // 126

        //
        // 135 : reserved for Solo until features merged with master
        //
        k_param_rtl_speed_cms = 135,
        k_param_fs_batt_curr_rtl,
        k_param_rtl_cone_slope, // 137

        //
        // 140: Sensor parameters
        //
        k_param_imu = 140, // deprecated - can be deleted
        k_param_battery_monitoring = 141,   // deprecated - can be deleted
        k_param_volt_div_ratio, // deprecated - can be deleted
        k_param_curr_amp_per_volt,  // deprecated - can be deleted
        k_param_input_voltage,  // deprecated - can be deleted
        k_param_pack_capacity,  // deprecated - can be deleted
        k_param_compass_enabled_deprecated,
        k_param_compass,
        k_param_rangefinder_enabled_old, // deprecated
        k_param_frame_type,
        k_param_optflow_enabled,    // deprecated
        k_param_fs_batt_voltage,    // unused - moved to AP_BattMonitor
        k_param_ch7_option_old,
        k_param_auto_slew_rate,     // deprecated - can be deleted
        k_param_rangefinder_type_old,     // deprecated
        k_param_super_simple = 155,
        k_param_axis_enabled = 157, // deprecated - remove with next eeprom number change
        k_param_copter_leds_mode,   // deprecated - remove with next eeprom number change
        k_param_ahrs, // AHRS group // 159

        //
        // 160: Navigation parameters
        //
        k_param_rtl_altitude = 160,
        k_param_crosstrack_gain,	// deprecated - remove with next eeprom number change
        k_param_rtl_loiter_time,
        k_param_rtl_alt_final,
        k_param_tilt_comp, 	//164	deprecated - remove with next eeprom number change


        //
        // Camera and mount parameters
        //
        k_param_camera = 165,
        k_param_camera_mount,
        k_param_camera_mount2,      // deprecated

        //
        // Battery monitoring parameters
        //
        k_param_battery_volt_pin = 168, // deprecated - can be deleted
        k_param_battery_curr_pin,   // 169 deprecated - can be deleted

        //
        // 170: Radio settings
        //
        k_param_rc_1_old = 170,
        k_param_rc_2_old,
        k_param_rc_3_old,
        k_param_rc_4_old,
        k_param_rc_5_old,
        k_param_rc_6_old,
        k_param_rc_7_old,
        k_param_rc_8_old,
        k_param_rc_10_old,
        k_param_rc_11_old,
        k_param_throttle_min,           // remove
        k_param_throttle_max,           // remove
        k_param_failsafe_throttle,
        k_param_throttle_fs_action,     // remove
        k_param_failsafe_throttle_value,
        k_param_throttle_trim,          // remove
        k_param_esc_calibrate,
        k_param_radio_tuning,
        k_param_radio_tuning_high_old,   // unused
        k_param_radio_tuning_low_old,    // unused
        k_param_rc_speed = 192,
        k_param_failsafe_battery_enabled, // unused - moved to AP_BattMonitor
        k_param_throttle_mid,           // remove
        k_param_failsafe_gps_enabled,   // remove
        k_param_rc_9_old,
        k_param_rc_12_old,
        k_param_failsafe_gcs,
        k_param_rcmap, // 199

        //
        // 200: flight modes
        //
        k_param_flight_mode1 = 200,
        k_param_flight_mode2,
        k_param_flight_mode3,
        k_param_flight_mode4,
        k_param_flight_mode5,
        k_param_flight_mode6,
        k_param_simple_modes,
        k_param_flight_mode_chan,

        //
        // 210: Waypoint data
        //
        k_param_waypoint_mode = 210, // remove
        k_param_command_total,       // remove
        k_param_command_index,       // remove
        k_param_command_nav_index,   // remove
        k_param_waypoint_radius,     // remove
        k_param_circle_radius,       // remove
        k_param_waypoint_speed_max,  // remove
        k_param_land_speed,
        k_param_auto_velocity_z_min, // remove
        k_param_auto_velocity_z_max, // remove - 219
        k_param_land_speed_high,

        //
        // 220: PI/D Controllers
        //
        k_param_acro_rp_p = 221,
        k_param_axis_lock_p,    // remove
        k_param_pid_rate_roll,      // remove
        k_param_pid_rate_pitch,     // remove
        k_param_pid_rate_yaw,       // remove
        k_param_p_stabilize_roll,   // remove
        k_param_p_stabilize_pitch,  // remove
        k_param_p_stabilize_yaw,    // remove
        k_param_p_pos_xy,           // remove
        k_param_p_loiter_lon,       // remove
        k_param_pid_loiter_rate_lat,    // remove
        k_param_pid_loiter_rate_lon,    // remove
        k_param_pid_nav_lat,        // remove
        k_param_pid_nav_lon,        // remove
        k_param_p_alt_hold,             // remove
        k_param_p_vel_z,                // remove
        k_param_pid_optflow_roll,       // remove
        k_param_pid_optflow_pitch,      // remove
        k_param_acro_balance_roll_old,  // remove
        k_param_acro_balance_pitch_old, // remove
        k_param_pid_accel_z,            // remove
        k_param_acro_balance_roll,
        k_param_acro_balance_pitch,
        k_param_acro_yaw_p,
        k_param_autotune_axis_bitmask, // remove
        k_param_autotune_aggressiveness, // remove
        k_param_pi_vel_xy,              // remove
        k_param_fs_ekf_action,
        k_param_rtl_climb_min,
        k_param_rpm_sensor,
        k_param_autotune_min_d, // remove
        k_param_arming, // 252  - AP_Arming
        k_param_logger = 253, // 253 - Logging Group

        // 254,255: reserved

        // k_param_i_throttle = 256,   
        // k_param_o_throttle,
        // k_param_a_throttle,
        // k_param_b_throttle,
        // k_param_c_throttle,
        // k_param_d_throttle,
        // k_param_e_throttle,
        // k_param_f_throttle,
        // k_param_g_throttle,
        // k_param_h_throttle,

        // k_param_j_throttle,
        // k_param_k_throttle,
        // k_param_l_throttle,
        // k_param_m_throttle,
        // k_param_n_throttle,
        // k_param_p_throttle,
        // k_param_q_throttle,
        // k_param_r_throttle,
        // k_param_s_throttle,
        // k_param_t_throttle,

        // k_param_throttle1,
        // k_param_throttle2,
        // k_param_throttle3,
        // k_param_throttle4,
        // k_param_throttle5,
        // k_param_throttle6,
        // k_param_throttle7,
        // k_param_throttle8,
        // k_param_throttle9,
        // k_param_throttle10,
        // k_param_throttle11,
        // k_param_throttle12,
        // k_param_throttle13,
        // k_param_throttle14,
        // k_param_throttle15,
        // k_param_throttle16,
        // k_param_throttle17,
        // k_param_throttle18,
        // k_param_throttle19,
        // k_param_throttle20,

        // k_param_throttle21,
        // k_param_throttle22,
        // k_param_throttle23,
        // k_param_throttle24,
        // k_param_throttle25,
        // k_param_throttle26,
        // k_param_throttle27,
        // k_param_throttle28,
        // k_param_throttle29,
        // k_param_throttle30,
        // k_param_throttle31,
        // k_param_throttle32,
        // k_param_throttle33,
        // k_param_throttle34,
        // k_param_throttle35,
        // k_param_throttle36,
        // k_param_throttle37,
        // k_param_throttle38,
        // k_param_throttle39,
        // k_param_throttle40,

        // k_param_throttle41,
        // k_param_throttle42,
        // k_param_throttle43,
        // k_param_throttle44,
        // k_param_throttle45,
        // k_param_throttle46,
        // k_param_throttle47,
        // k_param_throttle48,
        // k_param_throttle49,
        // k_param_throttle50,
        // k_param_throttle51,
        // k_param_throttle52,
        // k_param_throttle53,
        // k_param_throttle54,
        // k_param_throttle55,
        // k_param_throttle56,
        // k_param_throttle57,
        // k_param_throttle58,
        // k_param_throttle59,
        // k_param_throttle60,

        // k_param_throttle61,
        // k_param_throttle62,
        // k_param_throttle63,
        // k_param_throttle64,
        // k_param_throttle65,
        // k_param_throttle66,
        // k_param_throttle67,
        // k_param_throttle68,
        // k_param_throttle69,
        // k_param_throttle70,
        // k_param_throttle71,
        // k_param_throttle72,
        // k_param_throttle73,
        // k_param_throttle74,
        // k_param_throttle75,
        // k_param_throttle76,
        // k_param_throttle77,
        // k_param_throttle78,
        // k_param_throttle79,
        // k_param_throttle80,

        // k_param_throttle81,
        // k_param_throttle82,
        // k_param_throttle83,
        // k_param_throttle84,
        // k_param_throttle85,
        // k_param_throttle86,
        // k_param_throttle87,
        // k_param_throttle88,
        // k_param_throttle89,
        // k_param_throttle90,
        // k_param_throttle91,
        // k_param_throttle92,
        // k_param_throttle93,
        // k_param_throttle94,
        // k_param_throttle95,
        // k_param_throttle96,
        // k_param_throttle97,
        // k_param_throttle98,
        // k_param_throttle99,
        // k_param_throttle100,

        // k_param_throttle101,
        // k_param_throttle102,
        // k_param_throttle103,
        // k_param_throttle104,
        // k_param_throttle105,
        // k_param_throttle106,
        // k_param_throttle107,
        // k_param_throttle108,
        // k_param_throttle109,
        // k_param_throttle110,
        // k_param_throttle111,
        // k_param_throttle112,
        // k_param_throttle113,
        // k_param_throttle114,
        // k_param_throttle115,
        // k_param_throttle116,
        // k_param_throttle117,
        // k_param_throttle118,
        // k_param_throttle119,
        // k_param_throttle120,

        // k_param_throttle121,
        // k_param_throttle122,
        // k_param_throttle123,
        // k_param_throttle124,
        // k_param_throttle125,
        // k_param_throttle126,
        // k_param_throttle127,
        // k_param_throttle128,
        // k_param_throttle129,
        // k_param_throttle130,
        // k_param_throttle131,
        // k_param_throttle132,
        // k_param_throttle133,
        // k_param_throttle134,
        // k_param_throttle135,
        // k_param_throttle136,
        // k_param_throttle137,
        // k_param_throttle138,
        // k_param_throttle139,
        // k_param_throttle140,

        // k_param_throttle141,
        // k_param_throttle142,
        // k_param_throttle143,
        // k_param_throttle144,
        // k_param_throttle145,
        // k_param_throttle146,
        // k_param_throttle147,
        // k_param_throttle148,
        // k_param_throttle149,
        // k_param_throttle150,
        // k_param_throttle151,
        // k_param_throttle152,
        // k_param_throttle153,
        // k_param_throttle154,
        // k_param_throttle155,
        // k_param_throttle156,
        // k_param_throttle157,
        // k_param_throttle158,
        // k_param_throttle159,
        // k_param_throttle160,

        // k_param_throttle161,
        // k_param_throttle162,
        // k_param_throttle163,
        // k_param_throttle164,
        // k_param_throttle165,
        // k_param_throttle166,
        // k_param_throttle167,
        // k_param_throttle168,
        // k_param_throttle169,
        // k_param_throttle170,
        // k_param_throttle171,
        // k_param_throttle172,
        // k_param_throttle173,
        // k_param_throttle174,
        // k_param_throttle175,
        // k_param_throttle176,
        // k_param_throttle177,
        // k_param_throttle178,
        // k_param_throttle179,
        // k_param_throttle180,

        // k_param_throttle181,
        // k_param_throttle182,
        // k_param_throttle183,
        // k_param_throttle184,
        // k_param_throttle185,
        // k_param_throttle186,
        // k_param_throttle187,
        // k_param_throttle188,
        // k_param_throttle189,
        // k_param_throttle190,
        // k_param_throttle191,
        // k_param_throttle192,
        // k_param_throttle193,
        // k_param_throttle194,
        // k_param_throttle195,
        // k_param_throttle196,
        // k_param_throttle197,
        // k_param_throttle198,
        // k_param_throttle199,
        // k_param_throttle200,

        // k_param_throttle201,
        // k_param_throttle202,
        // k_param_throttle203,
        // k_param_throttle204,
        // k_param_throttle205,
        // k_param_throttle206,
        // k_param_throttle207,
        // k_param_throttle208,
        // k_param_throttle209,
        // k_param_throttle210,
        // k_param_throttle211,
        // k_param_throttle212,
        // k_param_throttle213,
        // k_param_throttle214,
        // k_param_throttle215,
        // k_param_throttle216,
        // k_param_throttle217,
        // k_param_throttle218,
        // k_param_throttle219,
        // k_param_throttle220,

        // k_param_throttle221,
        // k_param_throttle222,
        // k_param_throttle223,
        // k_param_throttle224,
        // k_param_throttle225,
        // k_param_throttle226,
        // k_param_throttle227,
        // k_param_throttle228,
        // k_param_throttle229,
        // k_param_throttle230,
        // k_param_throttle231,
        // k_param_throttle232,
        // k_param_throttle233,
        // k_param_throttle234,

        //k_param_g3, // 3rd block of parameters


        // the k_param_* space is 9-bits in size
        // 511: reserved
    };



    AP_Int16        format_version;

    // Telemetry control
    //
    AP_Int16        sysid_this_mav;
    AP_Int16        sysid_my_gcs;
    AP_Int8         telem_delay;

    AP_Float        throttle_filt;
    AP_Int16        throttle_behavior;
    AP_Float        pilot_takeoff_alt;

    AP_Int16        rtl_altitude;
    AP_Int16        rtl_speed_cms;
    AP_Float        rtl_cone_slope;
#if RANGEFINDER_ENABLED == ENABLED
    AP_Float        rangefinder_gain;
#endif

    AP_Int8         failsafe_gcs;               // ground station failsafe behavior
    AP_Int16        gps_hdop_good;              // GPS Hdop value at or below this value represent a good position

    AP_Int8         super_simple;
    AP_Int16        rtl_alt_final;
    AP_Int16        rtl_climb_min;              // rtl minimum climb in cm

    AP_Int8         wp_yaw_behavior;            // controls how the autopilot controls yaw during missions

    AP_Int16        poshold_brake_rate;         // PosHold flight mode's rotation rate during braking in deg/sec
    AP_Int16        poshold_brake_angle_max;    // PosHold flight mode's max lean angle during braking in centi-degrees
    
    // Waypoints
    //
    AP_Int32        rtl_loiter_time;
    AP_Int16        land_speed;
    AP_Int16        land_speed_high;
    AP_Int16        pilot_speed_up;    // maximum vertical ascending velocity the pilot may request
    AP_Int16        pilot_accel_z;               // vertical acceleration the pilot may request

    // Throttle
    //
    AP_Int8         failsafe_throttle;
    AP_Int16        failsafe_throttle_value;
    AP_Int16        throttle_deadzone;

    // Flight modes
    //
    AP_Int8         flight_mode1;
    AP_Int8         flight_mode2;
    AP_Int8         flight_mode3;
    AP_Int8         flight_mode4;
    AP_Int8         flight_mode5;
    AP_Int8         flight_mode6;
    AP_Int8         simple_modes;
    AP_Int8         flight_mode_chan;

    // Misc
    //
    AP_Int32        log_bitmask;
    AP_Int8         esc_calibrate;
    AP_Int8         radio_tuning;
    AP_Int8         frame_type;
    AP_Int8         disarm_delay;

    AP_Int8         land_repositioning;
    AP_Int8         fs_ekf_action;
    AP_Int8         fs_crash_check;
    AP_Float        fs_ekf_thresh;
    AP_Int16        gcs_pid_mask;

#if MODE_THROW_ENABLED == ENABLED
    AP_Int8         throw_motor_start;
#endif

#if AP_TERRAIN_AVAILABLE && AC_TERRAIN
    AP_Int8         terrain_follow;
#endif

    AP_Int16                rc_speed; // speed of fast RC Channels in Hz

    // Acro parameters
    AP_Float                acro_rp_p;
    AP_Float                acro_yaw_p;
    AP_Float                acro_balance_roll;
    AP_Float                acro_balance_pitch;
    AP_Int8                 acro_trainer;
    AP_Float                acro_rp_expo;

    // custom vars added by chris
    // AP_Float i_throttle;
    // AP_Float o_throttle;
    // AP_Int16 a_throttle;
    // AP_Int16 b_throttle;
    // AP_Int16 c_throttle;
    // AP_Int16 d_throttle;
    // AP_Int16 e_throttle;
    // AP_Int16 f_throttle;
    // AP_Int16 g_throttle;
    // AP_Int16 h_throttle;

    // AP_Int16 j_throttle;
    // AP_Int16 k_throttle;
    // AP_Int16 l_throttle;
    // AP_Int16 m_throttle;
    // AP_Int16 n_throttle;
    // AP_Int16 p_throttle;
    // AP_Int16 q_throttle;
    // AP_Int16 r_throttle;
    // AP_Int16 s_throttle;
    // AP_Int16 t_throttle;

    // AP_Int16 throttle1;
    // AP_Int16 throttle2;
    // AP_Int16 throttle3;
    // AP_Int16 throttle4;
    // AP_Int16 throttle5;
    // AP_Int16 throttle6;
    // AP_Int16 throttle7;
    // AP_Int16 throttle8;
    // AP_Int16 throttle9;
    // AP_Int16 throttle10;
    // AP_Int16 throttle11;
    // AP_Int16 throttle12;
    // AP_Int16 throttle13;
    // AP_Int16 throttle14;
    // AP_Int16 throttle15;
    // AP_Int16 throttle16;
    // AP_Int16 throttle17;
    // AP_Int16 throttle18;
    // AP_Int16 throttle19;
    // AP_Int16 throttle20;

    // AP_Int16 throttle21;
    // AP_Int16 throttle22;
    // AP_Int16 throttle23;
    // AP_Int16 throttle24;
    // AP_Int16 throttle25;
    // AP_Int16 throttle26;
    // AP_Int16 throttle27;
    // AP_Int16 throttle28;
    // AP_Int16 throttle29;
    // AP_Int16 throttle30;
    // AP_Int16 throttle31;
    // AP_Int16 throttle32;
    // AP_Int16 throttle33;
    // AP_Int16 throttle34;
    // AP_Int16 throttle35;
    // AP_Int16 throttle36;
    // AP_Int16 throttle37;
    // AP_Int16 throttle38;
    // AP_Int16 throttle39;
    // AP_Int16 throttle40;

    // AP_Int16 throttle41;
    // AP_Int16 throttle42;
    // AP_Int16 throttle43;
    // AP_Int16 throttle44;
    // AP_Int16 throttle45;
    // AP_Int16 throttle46;
    // AP_Int16 throttle47;
    // AP_Int16 throttle48;
    // AP_Int16 throttle49;
    // AP_Int16 throttle50;
    // AP_Int16 throttle51;
    // AP_Int16 throttle52;
    // AP_Int16 throttle53;
    // AP_Int16 throttle54;
    // AP_Int16 throttle55;
    // AP_Int16 throttle56;
    // AP_Int16 throttle57;
    // AP_Int16 throttle58;
    // AP_Int16 throttle59;
    // AP_Int16 throttle60;

    // AP_Int16 throttle61;
    // AP_Int16 throttle62;
    // AP_Int16 throttle63;
    // AP_Int16 throttle64;
    // AP_Int16 throttle65;
    // AP_Int16 throttle66;
    // AP_Int16 throttle67;
    // AP_Int16 throttle68;
    // AP_Int16 throttle69;
    // AP_Int16 throttle70;
    // AP_Int16 throttle71;
    // AP_Int16 throttle72;
    // AP_Int16 throttle73;
    // AP_Int16 throttle74;
    // AP_Int16 throttle75;
    // AP_Int16 throttle76;
    // AP_Int16 throttle77;
    // AP_Int16 throttle78;
    // AP_Int16 throttle79;
    // AP_Int16 throttle80;

    // AP_Int16 throttle81;
    // AP_Int16 throttle82;
    // AP_Int16 throttle83;
    // AP_Int16 throttle84;
    // AP_Int16 throttle85;
    // AP_Int16 throttle86;
    // AP_Int16 throttle87;
    // AP_Int16 throttle88;
    // AP_Int16 throttle89;
    // AP_Int16 throttle90;
    // AP_Int16 throttle91;
    // AP_Int16 throttle92;
    // AP_Int16 throttle93;
    // AP_Int16 throttle94;
    // AP_Int16 throttle95;
    // AP_Int16 throttle96;
    // AP_Int16 throttle97;
    // AP_Int16 throttle98;
    // AP_Int16 throttle99;
    // AP_Int16 throttle100;

    // AP_Int16 throttle101;
    // AP_Int16 throttle102;
    // AP_Int16 throttle103;
    // AP_Int16 throttle104;
    // AP_Int16 throttle105;
    // AP_Int16 throttle106;
    // AP_Int16 throttle107;
    // AP_Int16 throttle108;
    // AP_Int16 throttle109;
    // AP_Int16 throttle110;
    // AP_Int16 throttle111;
    // AP_Int16 throttle112;
    // AP_Int16 throttle113;
    // AP_Int16 throttle114;
    // AP_Int16 throttle115;
    // AP_Int16 throttle116;
    // AP_Int16 throttle117;
    // AP_Int16 throttle118;
    // AP_Int16 throttle119;
    // AP_Int16 throttle120;

    // AP_Int16 throttle121;
    // AP_Int16 throttle122;
    // AP_Int16 throttle123;
    // AP_Int16 throttle124;
    // AP_Int16 throttle125;
    // AP_Int16 throttle126;
    // AP_Int16 throttle127;
    // AP_Int16 throttle128;
    // AP_Int16 throttle129;
    // AP_Int16 throttle130;
    // AP_Int16 throttle131;
    // AP_Int16 throttle132;
    // AP_Int16 throttle133;
    // AP_Int16 throttle134;
    // AP_Int16 throttle135;
    // AP_Int16 throttle136;
    // AP_Int16 throttle137;
    // AP_Int16 throttle138;
    // AP_Int16 throttle139;
    // AP_Int16 throttle140;

    // AP_Int16 throttle141;
    // AP_Int16 throttle142;
    // AP_Int16 throttle143;
    // AP_Int16 throttle144;
    // AP_Int16 throttle145;
    // AP_Int16 throttle146;
    // AP_Int16 throttle147;
    // AP_Int16 throttle148;
    // AP_Int16 throttle149;
    // AP_Int16 throttle150;
    // AP_Int16 throttle151;
    // AP_Int16 throttle152;
    // AP_Int16 throttle153;
    // AP_Int16 throttle154;
    // AP_Int16 throttle155;
    // AP_Int16 throttle156;
    // AP_Int16 throttle157;
    // AP_Int16 throttle158;
    // AP_Int16 throttle159;
    // AP_Int16 throttle160;

    // AP_Int16 throttle161;
    // AP_Int16 throttle162;
    // AP_Int16 throttle163;
    // AP_Int16 throttle164;
    // AP_Int16 throttle165;
    // AP_Int16 throttle166;
    // AP_Int16 throttle167;
    // AP_Int16 throttle168;
    // AP_Int16 throttle169;
    // AP_Int16 throttle170;
    // AP_Int16 throttle171;
    // AP_Int16 throttle172;
    // AP_Int16 throttle173;
    // AP_Int16 throttle174;
    // AP_Int16 throttle175;
    // AP_Int16 throttle176;
    // AP_Int16 throttle177;
    // AP_Int16 throttle178;
    // AP_Int16 throttle179;
    // AP_Int16 throttle180;

    // AP_Int16 throttle181;
    // AP_Int16 throttle182;
    // AP_Int16 throttle183;
    // AP_Int16 throttle184;
    // AP_Int16 throttle185;
    // AP_Int16 throttle186;
    // AP_Int16 throttle187;
    // AP_Int16 throttle188;
    // AP_Int16 throttle189;
    // AP_Int16 throttle190;
    // AP_Int16 throttle191;
    // AP_Int16 throttle192;
    // AP_Int16 throttle193;
    // AP_Int16 throttle194;
    // AP_Int16 throttle195;
    // AP_Int16 throttle196;
    // AP_Int16 throttle197;
    // AP_Int16 throttle198;
    // AP_Int16 throttle199;
    // AP_Int16 throttle200;

    // AP_Int16 throttle201;
    // AP_Int16 throttle202;
    // AP_Int16 throttle203;
    // AP_Int16 throttle204;
    // AP_Int16 throttle205;
    // AP_Int16 throttle206;
    // AP_Int16 throttle207;
    // AP_Int16 throttle208;
    // AP_Int16 throttle209;
    // AP_Int16 throttle210;
    // AP_Int16 throttle211;
    // AP_Int16 throttle212;
    // AP_Int16 throttle213;
    // AP_Int16 throttle214;
    // AP_Int16 throttle215;
    // AP_Int16 throttle216;
    // AP_Int16 throttle217;
    // AP_Int16 throttle218;
    // AP_Int16 throttle219;
    // AP_Int16 throttle220;

    // AP_Int16 throttle221;
    // AP_Int16 throttle222;
    // AP_Int16 throttle223;
    // AP_Int16 throttle224;
    // AP_Int16 throttle225;
    // AP_Int16 throttle226;
    // AP_Int16 throttle227;
    // AP_Int16 throttle228;
    // AP_Int16 throttle229;
    // AP_Int16 throttle230;
    // AP_Int16 throttle231;
    // AP_Int16 throttle232;
    // AP_Int16 throttle233;
    // AP_Int16 throttle234;


    // Note: keep initializers here in the same order as they are declared
    // above.
    Parameters()
    {
    }
};

/*
  2nd block of parameters, to avoid going past 256 top level keys
 */
class ParametersG2 {
public:
    ParametersG2(void);

    // var_info for holding Parameter information
    static const struct AP_Param::GroupInfo var_info[];

    // altitude at which nav control can start in takeoff
    AP_Float wp_navalt_min;

    // button checking
    AP_Button button;

#if STATS_ENABLED == ENABLED
    // vehicle statistics
    AP_Stats stats;
#endif

#if GRIPPER_ENABLED
    AP_Gripper gripper;
#endif

#if MODE_THROW_ENABLED == ENABLED
    // Throw mode parameters
    AP_Int8 throw_nextmode;
    AP_Int8 throw_type;
#endif

    // ground effect compensation enable/disable
    AP_Int8 gndeffect_comp_enabled;

    // temperature calibration handling
    AP_TempCalibration temp_calibration;

#if BEACON_ENABLED == ENABLED
    // beacon (non-GPS positioning) library
    AP_Beacon beacon;
#endif

#if VISUAL_ODOMETRY_ENABLED == ENABLED
    // Visual Odometry camera
    AP_VisualOdom visual_odom;
#endif

#if PROXIMITY_ENABLED == ENABLED
    // proximity (aka object avoidance) library
    AP_Proximity proximity;
#endif

    // whether to enforce acceptance of packets only from sysid_my_gcs
    AP_Int8 sysid_enforce;
    
#if ADVANCED_FAILSAFE == ENABLED
    // advanced failsafe library
    AP_AdvancedFailsafe_Copter afs;
#endif

    // developer options
    AP_Int32 dev_options;

    // acro exponent parameters
    AP_Float acro_y_expo;
#if MODE_ACRO_ENABLED == ENABLED
    AP_Float acro_thr_mid;
#endif

    // frame class
    AP_Int8 frame_class;

    // RC input channels
    RC_Channels_Copter rc_channels;
    
    // control over servo output ranges
    SRV_Channels servo_channels;

#if MODE_SMARTRTL_ENABLED == ENABLED
    // Safe RTL library
    AP_SmartRTL smart_rtl;
#endif

    // wheel encoder and winch
#if WINCH_ENABLED == ENABLED
    AP_WheelEncoder wheel_encoder;
    AP_Winch winch;
#endif

    // Additional pilot velocity items
    AP_Int16    pilot_speed_dn;

    // Land alt final stage
    AP_Int16 land_alt_low;

#if TOY_MODE_ENABLED == ENABLED
    ToyMode toy_mode;
#endif

#if OPTFLOW == ENABLED
    // we need a pointer to the mode for the G2 table
    void *mode_flowhold_ptr;
#endif

#if MODE_FOLLOW_ENABLED == ENABLED
    // follow
    AP_Follow follow;
#endif

#ifdef USER_PARAMS_ENABLED
    // User custom parameters
    UserParameters user_parameters;
#endif

#if AUTOTUNE_ENABLED == ENABLED
    // we need a pointer to autotune for the G2 table
    void *autotune_ptr;
#endif

#ifdef ENABLE_SCRIPTING
    AP_Scripting scripting;
#endif // ENABLE_SCRIPTING

    AP_Float tuning_min;
    AP_Float tuning_max;

    // // custom vars added by chris
    // AP_Int8 test_var1;
    // AP_Int8 test_var2;
    // AP_Int8 test_var3;
    // AP_Int8 test_var4;
    // AP_Int8 test_var5;
    // AP_Int8 test_var6;
    // AP_Int8 test_var7;
    // AP_Int8 test_var8;
    // AP_Int8 test_var9;
    // AP_Int8 test_var10;

    // AP_Int8 test_var11;
    // AP_Int8 test_var12;
    // AP_Int8 test_var13;
    // AP_Int8 test_var14;
    // AP_Int8 test_var15;
    // AP_Int8 test_var16;
    // AP_Int8 test_var17;
    // AP_Int8 test_var18;
    // AP_Int8 test_var19;
    // AP_Int8 test_var20;

    // AP_Int8 test_var21;
    // AP_Int8 test_var22;
    // AP_Int8 test_var23;
    // AP_Int8 test_var24;
    // AP_Int8 test_var25;
    // AP_Int8 test_var26;
    // AP_Int8 test_var27;
    // AP_Int8 test_var28;
    // AP_Int8 test_var29;
    // AP_Int8 test_var30;

    // AP_Int8 test_var31;
    // AP_Int8 test_var32;
    // AP_Int8 test_var33;
    // AP_Int8 test_var34;
    // AP_Int8 test_var35;
    // AP_Float test_var36;
    // AP_Float test_var37;
    // AP_Float test_var38;
    // AP_Float test_var39;
    // AP_Float test_var40;

    // AP_Int16 test_var41;
    // AP_Int16 test_var42;
    // AP_Int16 test_var43;
    // AP_Int16 test_var44;
    // AP_Int16 test_var45;
    // AP_Int16 test_var46;
    // AP_Int16 test_var47;
    // AP_Int16 test_var48;
    // AP_Int16 test_var49;
    // AP_Int16 test_var50;
    
};

/*
  3rd block of parameters, to avoid going past 256 top level keys
 */
// class ParametersG3 {
// public:
//     ParametersG3(void);

//     // var_info for holding Parameter information
//     static const struct AP_Param::GroupInfo var_info[];

//     AP_Float test_var51;
//     AP_Float test_var52;
//     AP_Float test_var53;
//     AP_Float test_var54;
//     AP_Float test_var55;
//     AP_Float test_var56;
//     AP_Float test_var57;
//     AP_Float test_var58;
//     AP_Float test_var59;
//     AP_Float test_var60;
// };

extern const AP_Param::Info        var_info[];
