#pragma once

#include <AP_Param/AP_Param.h>

class UserParameters {

public:
    UserParameters() {}
    static const struct AP_Param::GroupInfo var_info[];
    
    // Put accessors to your parameter variables here
    // UserCode usage example: g2.user_parameters.get_int8Param()
    AP_Int16 get_param1Param() const { return _param1; }
    AP_Int16 get_param2Param() const { return _param2; }
    AP_Int16 get_param3Param() const { return _param3; }
    AP_Int16 get_param4Param() const { return _param4; }
    AP_Int16 get_param5Param() const { return _param5; }
    AP_Int16 get_param6Param() const { return _param6; }
    AP_Int16 get_param7Param() const { return _param7; }
    AP_Int16 get_param8Param() const { return _param8; }
    AP_Int16 get_param9Param() const { return _param9; }
    AP_Int16 get_param10Param() const { return _param10; }
    // AP_Int16 get_param11Param() const { return _param11; }
    // AP_Int16 get_param12Param() const { return _param12; }
    // AP_Int16 get_param13Param() const { return _param13; }
    // AP_Int16 get_param14Param() const { return _param14; }
    // AP_Int16 get_param15Param() const { return _param15; }
    // AP_Int16 get_param16Param() const { return _param16; }
    // AP_Int16 get_param17Param() const { return _param17; }
    // AP_Int16 get_param18Param() const { return _param18; }
    // AP_Int16 get_param19Param() const { return _param19; }
    // AP_Int16 get_param20Param() const { return _param20; }
    // AP_Int16 get_param21Param() const { return _param21; }
    // AP_Int16 get_param22Param() const { return _param22; }
    // AP_Int16 get_param23Param() const { return _param23; }
    // AP_Int16 get_param24Param() const { return _param24; }
    // AP_Int16 get_param25Param() const { return _param25; }
    // AP_Int16 get_param26Param() const { return _param26; }
    // AP_Int16 get_param27Param() const { return _param27; }
    // AP_Int16 get_param28Param() const { return _param28; }
    // AP_Int16 get_param29Param() const { return _param29; }
    // AP_Int16 get_param30Param() const { return _param30; }
    // AP_Int16 get_param31Param() const { return _param31; }
    // AP_Int16 get_param32Param() const { return _param32; }
    // AP_Int16 get_param33Param() const { return _param33; }
    // AP_Int16 get_param34Param() const { return _param34; }
    // AP_Int16 get_param35Param() const { return _param35; }
    // AP_Int16 get_param36Param() const { return _param36; }
    // AP_Int16 get_param37Param() const { return _param37; }
    // AP_Int16 get_param38Param() const { return _param38; }
    // AP_Int16 get_param39Param() const { return _param39; }
    // AP_Int16 get_param40Param() const { return _param40; }
    // AP_Int16 get_param41Param() const { return _param41; }
    // AP_Int16 get_param42Param() const { return _param42; }
    // AP_Int16 get_param43Param() const { return _param43; }
    // AP_Int16 get_param44Param() const { return _param44; }
    // AP_Int16 get_param45Param() const { return _param45; }
    // AP_Int16 get_param46Param() const { return _param46; }
    // AP_Int16 get_param47Param() const { return _param47; }
    // AP_Int16 get_param48Param() const { return _param48; }
    // AP_Int16 get_param49Param() const { return _param49; }
    // AP_Int16 get_param50Param() const { return _param50; }
    // AP_Int16 get_param51Param() const { return _param51; }
    // AP_Int16 get_param52Param() const { return _param52; }
    // AP_Int16 get_param53Param() const { return _param53; }
    // AP_Int16 get_param54Param() const { return _param54; }
    // AP_Int16 get_param55Param() const { return _param55; }
    // AP_Int16 get_param56Param() const { return _param56; }
    // AP_Int16 get_param57Param() const { return _param57; }
    // AP_Int16 get_param58Param() const { return _param58; }
    // AP_Int16 get_param59Param() const { return _param59; }
    // AP_Int16 get_param60Param() const { return _param60; }
    // AP_Int16 get_param61Param() const { return _param61; }
    // AP_Int16 get_param62Param() const { return _param62; }
    // AP_Int16 get_param63Param() const { return _param63; }
    // AP_Int16 get_param64Param() const { return _param64; }
    // AP_Int16 get_param65Param() const { return _param65; }
    // AP_Int16 get_param66Param() const { return _param66; }
    // AP_Int16 get_param67Param() const { return _param67; }
    // AP_Int16 get_param68Param() const { return _param68; }
    // AP_Int16 get_param69Param() const { return _param69; }
    // AP_Int16 get_param70Param() const { return _param70; }
    // AP_Int16 get_param71Param() const { return _param71; }
    // AP_Int16 get_param72Param() const { return _param72; }
    // AP_Int16 get_param73Param() const { return _param73; }
    // AP_Int16 get_param74Param() const { return _param74; }
    // AP_Int16 get_param75Param() const { return _param75; }
    // AP_Int16 get_param76Param() const { return _param76; }
    // AP_Int16 get_param77Param() const { return _param77; }
    // AP_Int16 get_param78Param() const { return _param78; }
    // AP_Int16 get_param79Param() const { return _param79; }
    // AP_Int16 get_param80Param() const { return _param80; }
    // AP_Int16 get_param81Param() const { return _param81; }
    // AP_Int16 get_param82Param() const { return _param82; }
    // AP_Int16 get_param83Param() const { return _param83; }
    // AP_Int16 get_param84Param() const { return _param84; }
    // AP_Int16 get_param85Param() const { return _param85; }
    // AP_Int16 get_param86Param() const { return _param86; }
    // AP_Int16 get_param87Param() const { return _param87; }
    // AP_Int16 get_param88Param() const { return _param88; }
    // AP_Int16 get_param89Param() const { return _param89; }
    // AP_Int16 get_param90Param() const { return _param90; }
    // AP_Int16 get_param91Param() const { return _param91; }
    // AP_Int16 get_param92Param() const { return _param92; }
    // AP_Int16 get_param93Param() const { return _param93; }
    // AP_Int16 get_param94Param() const { return _param94; }
    // AP_Int16 get_param95Param() const { return _param95; }
    // AP_Int16 get_param96Param() const { return _param96; }
    // AP_Int16 get_param97Param() const { return _param97; }
    // AP_Int16 get_param98Param() const { return _param98; }
    // AP_Int16 get_param99Param() const { return _param99; }
    // AP_Int16 get_param100Param() const { return _param100; }

    void set_param1Param(int16_t param1) { _param1 = param1; }
    void set_param2Param(int16_t param2) { _param2 = param2; }
    void set_param3Param(int16_t param3) { _param3 = param3; }
    void set_param4Param(int16_t param4) { _param4 = param4; }
    void set_param5Param(int16_t param5) { _param5 = param5; }
    void set_param6Param(int16_t param6) { _param6 = param6; }
    void set_param7Param(int16_t param7) { _param7 = param7; }
    void set_param8Param(int16_t param8) { _param8 = param8; }
    void set_param9Param(int16_t param9) { _param9 = param9; }
    void set_param10Param(int16_t param10) { _param10 = param10; }
    // void set_param11Param(int16_t param11) { _param11 = param11; }
    // void set_param12Param(int16_t param12) { _param12 = param12; }
    // void set_param13Param(int16_t param13) { _param13 = param13; }
    // void set_param14Param(int16_t param14) { _param14 = param14; }
    // void set_param15Param(int16_t param15) { _param15 = param15; }
    // void set_param16Param(int16_t param16) { _param16 = param16; }
    // void set_param17Param(int16_t param17) { _param17 = param17; }
    // void set_param18Param(int16_t param18) { _param18 = param18; }
    // void set_param19Param(int16_t param19) { _param19 = param19; }
    // void set_param20Param(int16_t param20) { _param20 = param20; }
    // void set_param21Param(int16_t param21) { _param21 = param21; }
    // void set_param22Param(int16_t param22) { _param22 = param22; }
    // void set_param23Param(int16_t param23) { _param23 = param23; }
    // void set_param24Param(int16_t param24) { _param24 = param24; }
    // void set_param25Param(int16_t param25) { _param25 = param25; }
    // void set_param26Param(int16_t param26) { _param26 = param26; }
    // void set_param27Param(int16_t param27) { _param27 = param27; }
    // void set_param28Param(int16_t param28) { _param28 = param28; }
    // void set_param29Param(int16_t param29) { _param29 = param29; }
    // void set_param30Param(int16_t param30) { _param30 = param30; }
    // void set_param31Param(int16_t param31) { _param31 = param31; }
    // void set_param32Param(int16_t param32) { _param32 = param32; }
    // void set_param33Param(int16_t param33) { _param33 = param33; }
    // void set_param34Param(int16_t param34) { _param34 = param34; }
    // void set_param35Param(int16_t param35) { _param35 = param35; }
    // void set_param36Param(int16_t param36) { _param36 = param36; }
    // void set_param37Param(int16_t param37) { _param37 = param37; }
    // void set_param38Param(int16_t param38) { _param38 = param38; }
    // void set_param39Param(int16_t param39) { _param39 = param39; }
    // void set_param40Param(int16_t param40) { _param40 = param40; }
    // void set_param41Param(int16_t param41) { _param41 = param41; }
    // void set_param42Param(int16_t param42) { _param42 = param42; }
    // void set_param43Param(int16_t param43) { _param43 = param43; }
    // void set_param44Param(int16_t param44) { _param44 = param44; }
    // void set_param45Param(int16_t param45) { _param45 = param45; }
    // void set_param46Param(int16_t param46) { _param46 = param46; }
    // void set_param47Param(int16_t param47) { _param47 = param47; }
    // void set_param48Param(int16_t param48) { _param48 = param48; }
    // void set_param49Param(int16_t param49) { _param49 = param49; }
    // void set_param50Param(int16_t param50) { _param50 = param50; }
    // void set_param51Param(int16_t param51) { _param51 = param51; }
    // void set_param52Param(int16_t param52) { _param52 = param52; }
    // void set_param53Param(int16_t param53) { _param53 = param53; }
    // void set_param54Param(int16_t param54) { _param54 = param54; }
    // void set_param55Param(int16_t param55) { _param55 = param55; }
    // void set_param56Param(int16_t param56) { _param56 = param56; }
    // void set_param57Param(int16_t param57) { _param57 = param57; }
    // void set_param58Param(int16_t param58) { _param58 = param58; }
    // void set_param59Param(int16_t param59) { _param59 = param59; }
    // void set_param60Param(int16_t param60) { _param60 = param60; }
    // void set_param61Param(int16_t param61) { _param61 = param61; }
    // void set_param62Param(int16_t param62) { _param62 = param62; }
    // void set_param63Param(int16_t param63) { _param63 = param63; }
    // void set_param64Param(int16_t param64) { _param64 = param64; }
    // void set_param65Param(int16_t param65) { _param65 = param65; }
    // void set_param66Param(int16_t param66) { _param66 = param66; }
    // void set_param67Param(int16_t param67) { _param67 = param67; }
    // void set_param68Param(int16_t param68) { _param68 = param68; }
    // void set_param69Param(int16_t param69) { _param69 = param69; }
    // void set_param70Param(int16_t param70) { _param70 = param70; }
    // void set_param71Param(int16_t param71) { _param71 = param71; }
    // void set_param72Param(int16_t param72) { _param72 = param72; }
    // void set_param73Param(int16_t param73) { _param73 = param73; }
    // void set_param74Param(int16_t param74) { _param74 = param74; }
    // void set_param75Param(int16_t param75) { _param75 = param75; }
    // void set_param76Param(int16_t param76) { _param76 = param76; }
    // void set_param77Param(int16_t param77) { _param77 = param77; }
    // void set_param78Param(int16_t param78) { _param78 = param78; }
    // void set_param79Param(int16_t param79) { _param79 = param79; }
    // void set_param80Param(int16_t param80) { _param80 = param80; }
    // void set_param81Param(int16_t param81) { _param81 = param81; }
    // void set_param82Param(int16_t param82) { _param82 = param82; }
    // void set_param83Param(int16_t param83) { _param83 = param83; }
    // void set_param84Param(int16_t param84) { _param84 = param84; }
    // void set_param85Param(int16_t param85) { _param85 = param85; }
    // void set_param86Param(int16_t param86) { _param86 = param86; }
    // void set_param87Param(int16_t param87) { _param87 = param87; }
    // void set_param88Param(int16_t param88) { _param88 = param88; }
    // void set_param89Param(int16_t param89) { _param89 = param89; }
    // void set_param90Param(int16_t param90) { _param80 = param90; }
    // void set_param91Param(int16_t param91) { _param81 = param91; }
    // void set_param92Param(int16_t param92) { _param82 = param92; }
    // void set_param93Param(int16_t param93) { _param83 = param93; }
    // void set_param94Param(int16_t param94) { _param84 = param94; }
    // void set_param95Param(int16_t param95) { _param85 = param95; }
    // void set_param96Param(int16_t param96) { _param86 = param96; }
    // void set_param97Param(int16_t param97) { _param87 = param97; }
    // void set_param98Param(int16_t param98) { _param88 = param98; }
    // void set_param99Param(int16_t param99) { _param89 = param99; }
    // void set_param100Param(int16_t param100) { _param100 = param100; }

private:
    // Put your parameter variable definitions here
    AP_Int16 _param1;
    AP_Int16 _param2;
    AP_Int16 _param3;
    AP_Int16 _param4;
    AP_Int16 _param5;
    AP_Int16 _param6;
    AP_Int16 _param7;
    AP_Int16 _param8;
    AP_Int16 _param9;
    AP_Int16 _param10;
    // AP_Int16 _param11;
    // AP_Int16 _param12;
    // AP_Int16 _param13;
    // AP_Int16 _param14;
    // AP_Int16 _param15;
    // AP_Int16 _param16;
    // AP_Int16 _param17;
    // AP_Int16 _param18;
    // AP_Int16 _param19;
    // AP_Int16 _param20;
    // AP_Int16 _param21;
    // AP_Int16 _param22;
    // AP_Int16 _param23;
    // AP_Int16 _param24;
    // AP_Int16 _param25;
    // AP_Int16 _param26;
    // AP_Int16 _param27;
    // AP_Int16 _param28;
    // AP_Int16 _param29;
    // AP_Int16 _param30;
    // AP_Int16 _param31;
    // AP_Int16 _param32;
    // AP_Int16 _param33;
    // AP_Int16 _param34;
    // AP_Int16 _param35;
    // AP_Int16 _param36;
    // AP_Int16 _param37;
    // AP_Int16 _param38;
    // AP_Int16 _param39;
    // AP_Int16 _param40;
    // AP_Int16 _param41;
    // AP_Int16 _param42;
    // AP_Int16 _param43;
    // AP_Int16 _param44;
    // AP_Int16 _param45;
    // AP_Int16 _param46;
    // AP_Int16 _param47;
    // AP_Int16 _param48;
    // AP_Int16 _param49;
    // AP_Int16 _param50;
    // AP_Int16 _param51;
    // AP_Int16 _param52;
    // AP_Int16 _param53;
    // AP_Int16 _param54;
    // AP_Int16 _param55;
    // AP_Int16 _param56;
    // AP_Int16 _param57;
    // AP_Int16 _param58;
    // AP_Int16 _param59;
    // AP_Int16 _param60;
    // AP_Int16 _param61;
    // AP_Int16 _param62;
    // AP_Int16 _param63;
    // AP_Int16 _param64;
    // AP_Int16 _param65;
    // AP_Int16 _param66;
    // AP_Int16 _param67;
    // AP_Int16 _param68;
    // AP_Int16 _param69;
    // AP_Int16 _param70;
    // AP_Int16 _param71;
    // AP_Int16 _param72;
    // AP_Int16 _param73;
    // AP_Int16 _param74;
    // AP_Int16 _param75;
    // AP_Int16 _param76;
    // AP_Int16 _param77;
    // AP_Int16 _param78;
    // AP_Int16 _param79;
    // AP_Int16 _param80;
    // AP_Int16 _param81;
    // AP_Int16 _param82;
    // AP_Int16 _param83;
    // AP_Int16 _param84;
    // AP_Int16 _param85;
    // AP_Int16 _param86;
    // AP_Int16 _param87;
    // AP_Int16 _param88;
    // AP_Int16 _param89;
    // AP_Int16 _param90;
    // AP_Int16 _param91;
    // AP_Int16 _param92;
    // AP_Int16 _param93;
    // AP_Int16 _param94;
    // AP_Int16 _param95;
    // AP_Int16 _param96;
    // AP_Int16 _param97;
    // AP_Int16 _param98;
    // AP_Int16 _param99;
    // AP_Int16 _param100;
};
